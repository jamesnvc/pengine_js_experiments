class Rxn {
  constructor(data) {
    this._data = data;
    this._cbs = [];

    for (const prop in data) {
      Object.defineProperty(
        this, prop,
        {get: function() {
          return this._data[prop];
        },
         set: function(v) {
           this._data[prop] = v;
           requestAnimationFrame(() => this.runCallbacks());
         }});
    }
  }

  runCallbacks() {
    this._cbs.forEach(cb => cb(this));
  }

  addCb(c) {
    this._cbs.push(c);
  }

};

class AppState {

  constructor() {
    this._callbacks = {};
    this._data = {};
  }

  addCallback(type, element, cb) {
    if (!this._callbacks[type]) { this._callbacks[type] = []; }
    this._callbacks[type].push(() => cb(type, element));
  }

  invokeCallbacks(key) {
    console.log("invoking callbacks", key);
    if (this._callbacks[key]) {
      requestAnimationFrame(
        () => this._callbacks[key].forEach(f => f()));
    }
  }

  get(key) {
    return this._data[key];
  }

  set(key, val) {
    this._data[key] = val;
    this.invokeCallbacks(key);
  }

  update(key, fn) {
    fn(this._data[key]);
    this.invokeCallbacks(key);
  }

};

const confForType = (type) => {
  if (rx_state[type]) {
    return [rx_state[type], type];
  }
  for (const key in rx_state) {
    const props = rx_state[key];
    if (props.element == type) {
      return [props, key];
    }
  }
  throw new Error(`Can't find conf for type ${type}`);
};

const _findUpdateQueryFor = (type) => {
  const [ conf, _ ] = confForType(type);
  return [conf.application, conf.update];
};

const buildUpdateFn = (type) => {
  const [app, query] = _findUpdateQueryFor(type);
  const matches = query.match(/\$\w+[.]\w+/g);
  if (!matches) { return ((_) => runQuery(app, query, () => true)); }
  return (obj) => {
    const q = query.replace(/\$\w+[.](\w+)/g, (_, prop) => obj[prop]);
    runQuery(app, q, () => true);
  };
};

const updateEltBody = (obj, parentElt) => {
  parentElt.querySelectorAll('[data-content]')
    .forEach((elt, idx, list) => {
      const k = elt.attributes['data-content'].value;
      elt.innerText = obj[k];
      obj.addCb((o) => {elt.innerText = o[k];});
    });
};

const updateEltCheckboxes = (obj, parentElt) => {
  const updateFn = buildUpdateFn(parentElt.attributes['data-item'].value);
  parentElt.querySelectorAll('[data-checked]')
    .forEach((elt, idx, list) => {
      const k = elt.attributes['data-checked'].value;
      elt.onchange = (e) => {
        obj[k] = elt.checked;
        // TODO: make this function able to depend on type?
        updateFn(obj);
      };
      elt.checked = obj[k];
      obj.addCb((o) => { elt.checked = o[k]; });
    });
};

const updateEltValue = (obj, parentElt) => {
  const updateFn = buildUpdateFn(parentElt.attributes['data-item'].value);
  parentElt.querySelectorAll('[data-value]')
    .forEach((elt, idx, list) => {
      const k = elt.attributes['data-value'].value;
      elt.onchange = (e) => {
        obj[k] = elt.value;
        // TODO: make this function able to depend on type?
        updateFn(obj);
      };
      elt.value = obj[k];
      obj.addCb((o) => { elt.value = o[k]; });
    });
};

const updateFns = [
  updateEltBody,
  updateEltCheckboxes,
  updateEltValue
];

let appState = new AppState();

const loadForeach = () => {
  document.querySelectorAll('[data-foreach]')
    .forEach((elt) => {
      const eltType = elt.attributes['data-foreach'].value;
      const conf = rx_state[eltType];
      runQuery(conf.application, conf.list,
               (result) => {
                 appState.addCallback(eltType, elt, displayForeach);
                 appState.set(eltType,
                              new Set(result[eltType].map(t => new Rxn(t))));
               });
    });
};

const displayForeach = (eltType, elt) => {
  const config = rx_state[eltType];
  let items = Array.from(appState.get(eltType));
  console.log('items', items);
  if (elt.attributes['data-sort-by']) {
    const attr = elt.attributes['data-sort-by'].value;
    items.sort((a, b) => {
      if (a[attr] < b[attr]) { return -1; }
      if (a[attr] > b[attr]) { return 1; }
      return 0;
    });
  }
  const itemName = config.element;
  const itemTemplate = elt.querySelector(`[data-item-template="${itemName}"]`);
  const itemUpdateQ = config.update;
  // TODO: fill in update template somehow, connect it up
  // so that the updateFns call that instead
  for (const item of items) {
    let newElt = elt.querySelector(`[data-id="${item.id}"]`);
    if (!newElt) {
      newElt = itemTemplate.cloneNode(true);
      elt.appendChild(newElt);
    }
    newElt.removeAttribute('data-item-template');
    newElt.setAttribute('data-item', itemName);
    // TODO: make id key configurable
    newElt.setAttribute('data-id', item.id);
    updateFns.forEach(f => f(item, newElt));
  }
};

const runQuery = (app, query, onsuccess) => {
  new Pengine({
    application: app,
    ask: query,
    onsuccess: function() { onsuccess(this.data[0]); },
    onfailure: function() { console.error('Failure'); },
    onerror: function(err) { console.error(err); }
  });
};

const setupOnClickQueries = () => {
  document.querySelectorAll('[data-onclick]')
    .forEach((elt) => {
      const query = elt.attributes['data-onclick'].value;
      const app = elt.attributes['data-application'].value;
      elt.addEventListener('click', (e) => {
        console.log("running query", query);
        runQuery(app, query, (resp) => {
          console.log("RESPONSE", resp);
          for (const prop in resp) {
            const [ conf, key ] = confForType(prop);
            if (conf) {
              if (appState.get(prop)) {
                appState.set(key, resp[prop]);
              } else {
                appState.update(key, (v) => v.add(new Rxn(resp[prop])));
              }
            }
          }
        });
      });
    });
};

document.addEventListener('DOMContentLoaded', function () {
  loadForeach();
  setupOnClickQueries();
});
