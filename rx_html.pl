:- module(rx_html, [rx_html//1]).

:- use_module(library(http/html_write), [html//1]).
:- use_module(library(http/json), [json_write_dict/3]).

:- module_transparent(rx_html//1).

rx_html(Spec) -->
    { qrx_html(Spec, Rxd), ! },
    html(Rxd).

qrx_html(Var, _) :-
    var(Var), !, instantiation_error(Var).
qrx_html([], []).
qrx_html([H|T], [RH|RT]) :-
    rx_html_expand(H, RH),
    qrx_html(T, RT).
qrx_html(Spec, ExSpec) :-
    rx_html_expand(Spec, ExSpec).

rx_html_expand(Tok, Tok) :- atomic(Tok).
rx_html_expand(\Term, \Term).
rx_html_expand(rx_state(State), StateJs) :-
    with_output_to(string(StateJSON),
                   json_write_dict(current_output, State, [])),
    StateJs = script([], ['window.rx_state =', StateJSON, ';']).
rx_html_expand(input(Attrs), input(ExAttrs)) :-
    rx_html_expand_attrs(Attrs, ExAttrs).
rx_html_expand(Term, ExTerm) :-
    ( Term =.. [Env, Attrs, Content]
    ; ( Term =.. [Env, Content], Attrs = []) ),
    Content = rx(Prop),
    ExContent = [],
    rx_html_expand_attrs(Attrs, ExAttrs_),
    ExAttrs = ['data-content'(Prop)|ExAttrs_],
    ExTerm =.. [Env, ExAttrs, ExContent].
rx_html_expand(Term, ExTerm) :-
    ( Term =.. [Env, Attrs, Content]
    ; ( Term =.. [Env, Content], Attrs = []) ),
    rx_html_expand_attrs(Attrs, ExAttrs),
    qrx_html(Content, ExContent),
    ExTerm =.. [Env, ExAttrs, ExContent].
rx_html_expand(Term, Term).

rx_html_expand_attrs([], []).
rx_html_expand_attrs([H|T], [ExH|ExT]) :-
    rx_html_expand_attr(H, ExH),
    rx_html_expand_attrs(T, ExT).
rx_html_expand_attrs(Attr, [ExAttr]) :-
    rx_html_expand_attr(Attr, ExAttr).

rx_html_expand_attr(rx_item(Item), 'data-item-template'(Item)).
rx_html_expand_attr(rx_foreach(Items), 'data-foreach'(Items)).
rx_html_expand_attr(rx_sort_by(Prop), 'data-sort-by'(Prop)).
rx_html_expand_attr(onclick_query(Q), 'data-onclick'(Q)).
rx_html_expand_attr(onclick_application(App), 'data-application'(App)).
rx_html_expand_attr(checked(rx(V)), 'data-checked'(V)).
rx_html_expand_attr(value(rx(Prop)), 'data-value'(Prop)).
rx_html_expand_attr(Attr, Attr).
