:- module(test_server, [go/1]).

:- use_module(library(http/thread_httpd), [http_server/2]).
:- use_module(library(http/http_dispatch), [http_dispatch/1,
                                            http_handler/3,
                                            http_redirect/3]).
:- use_module(library(http/http_session), [http_set_session_options/1,
                                           http_session_asserta/1,
                                           http_session_data/1,
                                           http_session_retractall/1]).
:- use_module(library(http/html_write), [reply_html_page/2,
                                         html//1,
                                         html_post//2,
                                         html_receive//1]).
:- use_module(library(http/http_parameters), [http_parameters/3]).
:- use_module(library(http/html_head), [html_requires//1, html_resource/2]).
:- use_module(library(http/http_files), [http_reply_from_files/3]).
:- use_module(library(pengines), []).
:- use_module(library(css_write), [css//1, write_css/2]).

:- use_module(rx_html, [rx_html//1]).
:- use_module(todo_db, [attach_todos_db/1,
                        list_todos/2]).
:- use_module(users, [attach_users_db/1,
                      authenticate_user/3]).

% Pengine config
:- use_module(app).

:- multifile pengines:authentication_hook/3.

pengines:authentication_hook(_Request, todo_db_app, User) :-
    logged_in(User), !.
pengines:authentication_hook(_Request, todo_db_app, _User) :-
    throw(http_reply(forbidden(/))).

:- multifile pengines:not_sandboxed/2.

pengines:not_sandboxed(_User, todo_db_app).

:- multifile pengines:prepare_goal/3.

pengines:prepare_goal(G, G, _Opts) :-
    functor(G, Func, Arity),
    memberchk(Func-Arity, [add_todo-2,
                           list_todos-1,
                           remove_todo-1,
                           update_todo-1,
                           remove_completed-0]).
pengines:prepare_goal(_, fail, _).

% HTTP Routes

:- multifile http:location/3.
:- dynamic   http:location/3.

http:location(js, '/js', []).
http:location(css, '/css', []).
http:location(img, '/img', []).
user:file_search_path(css, './css').
user:file_search_path(js, './js').
user:file_search_path(icons, './icons').

:- http_handler(/, logged_in_page(home_page), []).
:- http_handler('/login', login_handler, []).
:- http_handler('/logout', logout_handler, []).

:- html_resource(style, [virtual(true), requires([css('style.css')]), mime_type(text/css)]).
:- html_resource(queries, [virtual(true), requires([js('queries.js')]), mime_type(text/javascript)]).

:- http_handler(js(.), http_reply_from_files('js/', []),
           [priority(1000), prefix]).
:- http_handler(css(.), http_reply_from_files('css/', []),
                [priority(1000), prefix]).
:- http_handler(img(.), http_reply_from_files('icons/', []),
                [priority(1000), prefix]).

% Main
go(Port) :-
    attach_todos_db('todos_data'),
    attach_users_db('users_data'),
    http_set_session_options([timeout(3600)]),
    http_server(http_dispatch, [port(Port)]).

% Login/out handlers

logged_in(User) :-
    http_session_data(logged_in(User)).

logged_in_page(Page, Request) :-
    ( logged_in(_)
    -> call(Page, Request)
    ; reply_html_page(
          [title('Test Server')],
          \login_body)
    ).

login_body -->
    html(div([
             div(h1('Test Server')),
             div(form([action('/login'), method('POST')],
                      [label(['User Name:',
                              input([type(text), name(uname)], [])]),
                       label(['Password:',
                              input([type(password), name(pw)])]),
                       input([type(submit), value(submit)])])
                )])).

login_handler(Request) :-
    http_parameters(Request, [], [form_data(Data)]),
    memberchk(uname=Uname, Data),
    memberchk(pw=Passwd, Data),
    authenticate_user(Uname, Passwd, UserId),
    http_session_asserta(logged_in(UserId)),
    !,
    http_redirect(moved, '/', Request).
login_handler(_Request) :-
    reply_html_page(
        [title('Test Server')],
        \login_body).

%% valid_credentials('james', 'foobar').
%% valid_credentials('test', 'foobar').

logout_handler(Request) :-
    memberchk(method(post), Request),
    http_session_retractall(logged_in(_)),
    !,
    http_redirect(moved, '/', Request).

% Main Handler

home_page(_Request) :-
    reply_html_page(
        [title('Test Server'),
         \html_receive(css)],
        \reactive_home_body).

home_body -->
    { logged_in(User),
      list_todos(User, Todos) },
    html([\html_requires(style),
          \html_requires(queries),
          div([id(main)],
              [\logout_button,
               p('Hello world'),
               % TODO: how to specify what to do with the result?
               button('data-onclick'('add_todo("New Todo", Todo)'), "+"),
               button('data-onclick'(markAllCompleted), "Mark All"),
               button('data-onclick'(unmarkAllCompleted), "Clear All"),
               % TODO: how to make this part declarative?
               ul(id(todos),
                  [\todo_list(Todos)]),
               button('data-onclick'(remove_completed),
                      "Remove Completed")]),
          div([id(templates)],
              [
                  \todo_element(todo{})
              ]),
          \pengine_lib_require
         ]).

todo_list([]) --> [].
todo_list([T|Ts]) -->
    todo_element(T), todo_list(Ts).

todo_element(Todo) -->
    { todo{
          id: Id,
          body: Body} :< Todo
      ; (Id = 'Id', Body = 'Body') },
    html(li(['data-id'(Id), 'data-type'(todo), class('todo-item')],
            [input([class(complete), type(checkbox), 'data-checked'(completed)]),
             input([class(editing), type(text), 'data-value'(body), value(Body)]),
             span([class(body), 'data-content'(body)], Body)])).

logout_button -->
    html(form([method(post), action('/logout')],
             input([type(submit), value(logout)]))).

pengine_lib_require -->
    html([script(src('https://code.jquery.com/jquery-3.3.1.min.js'), []),
          %% script(src('/js/pengines_fork.js'), [])
          script(src('/pengine/pengines.js'), [])
         ]).

todo_items_css -->
    css(['ul[data-foreach="Todos"]'(
             margin('2em'),
             [li('list-style-type'(none)),
              '[data-item="Todo"]'(
                  [],
                  ['.editing'(display(none)),
                   '.body'('font-family'(monospace)),
                   '&:hover'(
                       [],
                       ['.editing'(display(inline)),
                        '.body'(display(none))])])])]).

main_css -->
    css([body(margin("3em")),
         '[data-item-template]'(display(none)),
         \(test_server:todo_items_css)
        ]).

include_css(CssDcg) -->
    { write_css(CssDcg, CssTxt),
      debug(xxx, "Including css ~s", [CssTxt]) },
    html_post(css, style([], CssTxt)).

reactive_home_body -->
    rx_html([\html_requires(queries),
             \include_css(main_css),

             rx_state(
                 _{'Todos':
                   _{application: 'todo_db_app',
                     element: 'Todo',
                     list: "list_todos(Todos)",
                     update: "update_todo(todo{id: '$Todo.id', body: \"$Todo.body\", completed: $Todo.completed})",
                     remove: "remove_todo('$Todo.id')"
                    }
                  }
             ),

             div(id(main),
                 [\logout_button,
                  button([class(addTodo),
                          onclick_application(todo_db_app),
                          % The name of the "return" variable here is important;
                          % corresponds to the `element` property in the state
                          onclick_query('add_todo("New Todo", Todo)')],
                        "Add"),
                  button([class(removeCompleted),
                          onclick_application(todo_db_app),
                          % TODO: need to refresh after query?
                          onclick_query(remove_completed)],
                        "Remove Completed"),
                  ul([rx_foreach('Todos'), rx_sort_by('created')],
                     li(rx_item('Todo'),
                        [input([class(complete), type(checkbox),
                                checked(rx(completed))]),
                         input([class(editing), type(text), value(rx(body))]),
                         span(class(body), rx(body))]))
                 ]),
             \pengine_lib_require
            ]).
