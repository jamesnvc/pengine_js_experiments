:- module(todo_db, [attach_todos_db/1,
                    add_todo/3,
                    remove_todo/2,
                    update_todo/2,
                    list_todos/2]).

:- use_module(library(persistency)).
:- use_module(library(uuid), [uuid/1]).

:- persistent todo_item(id:atom,
                        user:atom,
                        body:string,
                        completed:boolean,
                        created:number).

attach_todos_db(File) :-
    db_attach(File, []).

add_todo(User, Body, NewTodo) :-
    uuid(Id), get_time(Now),
    assert_todo_item(Id, User, Body, false, Now),
    db_sync(_),
    NewTodo = todo{id: Id,
                   user: User,
                   body: Body,
                   completed: false,
                   created: Now}.

remove_todo(User, TodoId) :-
    retract_todo_item(TodoId, User, _, _, _).

remove_completed(User) :-
    retractall_todo_item(_, User, _, true, _).

update_todo(User, Todo) :-
    todo{id: TodoId,
         body: NewBody,
         completed: NewCompleted} :< Todo,
    with_mutex(todo_db,
               (todo_item(TodoId, User, _, _, Created),
                retractall_todo_item(TodoId, _, _, _, _),
                assert_todo_item(TodoId, User, NewBody, NewCompleted, Created))).

list_todos(User, Todos) :-
    findall(Todo,
            (todo_item(Id, User, Body, Status, Created),
             Todo = todo{id: Id,
                         user: User,
                         body: Body,
                         completed: Status,
                         created: Created}),
            Todos_),
    sort(created, @=<, Todos_, Todos).
