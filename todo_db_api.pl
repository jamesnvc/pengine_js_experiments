:- module(todo_db_api, [add_todo/2,
                        remove_todo/1,
                        update_todo/1,
                        list_todos/1,
                        remove_completed/0]).

:- use_module(library(pengines), [pengine_user/1]).
:- use_module(todo_db, []).

add_todo(Body, NewTodo) :-
    pengine_user(User),
    todo_db:add_todo(User, Body, NewTodo).

remove_todo(TodoId) :-
    pengine_user(User),
    todo_db:remove_todo(User, TodoId).

remove_completed :-
    pengine_user(User),
    todo_db:remove_completed(User).

update_todo(Todo) :-
    pengine_user(User),
    todo_db:update_todo(User, Todo).

list_todos(Todos) :-
    pengine_user(User),
    todo_db:list_todos(User, Todos).
