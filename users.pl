:- module(users, [attach_users_db/1,
                  register_user/3,
                  authenticate_user/3]).

:- use_module(library(persistency)).
:- use_module(library(uuid), [uuid/1]).
:- use_module(library(crypto), [crypto_password_hash/2]).

:- persistent user_info(id:atom,
                        name:atom,
                        pass_hash:atom).

attach_users_db(File) :-
    db_attach(File, []).

%% register_user(+Name, +Password, -Id) is det
register_user(Name, Password, Id) :-
    \+ user_info(_, Name, _),
    uuid(Id),
    crypto_password_hash(Password, Hash),
    assert_user_info(Id, Name, Hash),
    db_sync(_).

%% authenticate(+Name, +Password, -Id) is semidet
authenticate_user(Name, Password, Id) :-
    user_info(Id, Name, Hash),
    crypto_password_hash(Password, Hash).
